package com.yuuki.demo.service.impl;

import com.yuuki.demo.entity.Employ;
import com.yuuki.demo.mapper.EmployMapper;
import com.yuuki.demo.service.IEmployService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
* <p>
    *  服务实现类
    * </p>
*
* @author yuuki
* @since 2020-02-13
*/
@Service
    public class EmployServiceImpl extends ServiceImpl<EmployMapper, Employ> implements IEmployService {

    }
