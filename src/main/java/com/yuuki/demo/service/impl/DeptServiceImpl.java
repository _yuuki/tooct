package com.yuuki.demo.service.impl;

import com.yuuki.demo.entity.Dept;
import com.yuuki.demo.mapper.DeptMapper;
import com.yuuki.demo.service.IDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
* <p>
    *  服务实现类
    * </p>
*
* @author yuuki
* @since 2020-02-13
*/
@Service
    public class DeptServiceImpl extends ServiceImpl<DeptMapper, Dept> implements IDeptService {

    }
