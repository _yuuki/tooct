package com.yuuki.demo.service;

import com.yuuki.demo.entity.Dept;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yuuki
 * @since 2020-02-13
 */
public interface IDeptService extends IService<Dept> {

}
