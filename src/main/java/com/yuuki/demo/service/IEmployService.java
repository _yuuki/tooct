package com.yuuki.demo.service;

import com.yuuki.demo.entity.Employ;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yuuki
 * @since 2020-02-13
 */
public interface IEmployService extends IService<Employ> {

}
