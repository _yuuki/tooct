package com.yuuki.demo.mapper;

import com.yuuki.demo.entity.Dept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* <p>
    *  Mapper 接口
    * </p>
*
* @author yuuki
* @since 2020-02-13
*/
    public interface DeptMapper extends BaseMapper<Dept> {

    }
