package com.yuuki.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {

    @RequestMapping
    public String index() {
        return "index";
    }

    @RequestMapping("/dept")
    public String dept() {
        return "dept";
    }
    @RequestMapping("/employ")
    public String employ() {
        return "employ";
    }
}
