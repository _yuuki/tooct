package com.yuuki.demo.controller;

import com.yuuki.demo.model.User;
import org.springframework.web.bind.annotation.*;

/**
 * 使用@RestController 标识这个类是个rest请求的controller，只返回json
 * 类上加@RequestMapping("/user")，方法上的url都会加上/user前缀
 */
@RestController
@RequestMapping("/user")
public class UserController {


    @RequestMapping("name")
    public String name(@RequestParam(name = "name") String name) {
        return name;
    }

    @RequestMapping("age")
    public String ageAndsex(String age,String sex) {
        return age+"-"+sex;
    }

    @RequestMapping("/pojo")
    public User user(@RequestBody User user) {
        return user;
    }

    /**
     *
     * @param name
     * @return
     */
    @GetMapping("/{name}")
    public User getUser(@PathVariable(name = "name") String name) {
        User user = new User();
        user.setName(name);
        return user;
    }

}
