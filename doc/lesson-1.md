#第一课

## 创建一个springboot工程
1. file->new->project
2. 选择spring initializr
![1](../img/create1.png)
2. 填写group id和artifact id
![2](../img/create2.png)
3. 选择模块以及spring boot版本，目前还是用2.1吧2.2的坑我还没踩，模块刚开始选择web模块就行
![3](../img/create3.png)
4. 完成，等待依赖下载完成

## 编写第一个controller
1. 新建一个包命名为controller
2. 在controller包下新建一个类UserController,代码如下
    ```java
    @Controller
    public class UserController {
    
        @RequestMapping("/hello")
        @ResponseBody
        public String hello() {
            return "hello spring boot";
        }
    
    }
    ```
3. 启动项目，访问 ``localhost:8080/hello``,会看到浏览器返回一个字符串**hello spring boot**

完成后恭喜，一个简单的springboot项目已经搭建完成了！

## spring及spring mvc的注解学习
spring中有非常非常非常多的注解，但是常用的就那么些，我们先学常用的，不常用的后面如果用到了再说

### 标记bean

注解|作用
---|---
@Controller|标记这个类是一个controller（controller层，默认返回页面）
@Service|标记这个类是一个service（service层）
@Repository|标记这个类是一个repository（dao层）
@Component|标记这个类是一个被spring管理的bean
@RestController|标记这个类是一个controller（被@Restcontroller标记的类默认返回json格式数据）
其实这四个注解的作用是一样的，主要是用于区分不同的bean 

### mvc注解

注解|作用
---|---
@RequestMapping|可以标记类也可以标记方法，可以接收任何http method（get，post...)，可以配置只接收某种方式的请求
@GetMapping|只能接收get请求
@PostMapping|只能接收post请求
@PutMapping|只能接收put请求
@DeleteMapping|只能接收delete请求
@PatchMapping|只能接收patch请求
@ResponseBody|标记这个方法返回json格式数据

除了@ResponseBody 都是用来做url映射的，作用一致

### 尝试下
修改下上面写的UserController
```java
/**
 * 使用@RestController 标识这个类是个rest请求的controller，只返回json
 * 类上加@RequestMapping("/user")，方法上的url都会加上/user前缀
 */
@RestController
@RequestMapping("/user")
public class UserController {

    /**
     * 因为类上加了/user，所以映射路径会变成/user/requestMapping
     * @return
     */
    @RequestMapping("requestMapping")
    public String requestMapping() {
        return "requestMapping";
    }
    /**
     * 指定了method为get，就只能接受get请求了
     * @return
     */
    @RequestMapping(value = "onlyGet",method = RequestMethod.GET)
    public String onlyGetRequestMapping() {
        return "onlyGetRequestMapping";
    }

    @PostMapping("postMapping")
    public String postMapping() {
        return "postMapping";
    }

}
```
用postman测试，其他注解自己试

接下来再试试返回页面
1. pom文件加上thymeleaf依赖
    ```xml
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-thymeleaf</artifactId>
    </dependency>
    ```
2. 在resource/templeates目录下新建一个页面index.html
3. 再新建一个controller，DeptController
    ```java
    /**
     * 这次我们用@Controller标记，让他可以返回页面
     */
    @Controller
    @RequestMapping("dept")
    public class DeptController {
    
    
        @RequestMapping("index")
        public String index(){
            //return 的字符串对应模板的名字 如：index对应我们刚刚新建的index.html
            return "index";
        }
    }
   ```
4. 访问``localhost:8080/dept/index``可以看到返回了我们刚刚新建的页面

ok，第一次课到此结束，下一节课我们学习如何接受参数
