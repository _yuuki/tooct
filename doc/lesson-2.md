# 第二课

上节课学到了一些日常使用比较频繁的注解，这节课继续，学一下参数接收

### 参数接收
参数接收的方式，一般前端传参就三种吧

- application/x-www-form-urlencoded 键值对形式
- application/json json流
- multipart/form-data 传文件用的，不传文件也能用- -
- 路径参数

然后我们分别用着三种传参方式来试试

#### 键值对形式
##### 初级
修改下上节课写的UserController
```java
@RequestMapping("name")
public String name(String name) {
    return name;
}

@RequestMapping("age")
public String ageAndsex(String age,String sex) {
    return age+"-"+sex;
}
```
然后用postman请求下接口，带上参数发现正确返回了，然后再试试不传，会发现啥都没返回也没报错。
##### 进阶
<br>接下来进阶一下，加上一个新的注解``@RequestParam``，代码就变成了
```java
@RequestMapping("name")
public String name(@RequestParam(name = "name") String name) {
    return name;
}
```
再试试，会发现带参数的时候还是正确的，不带参数的时候就会返回一个400 bad request的错误，那我们就唠唠这个注解

``@RequestParm``这个注解是用来标记入参的，可以给他传四个值

值|作用
---|---
name|参数的key name
value|参数的key name 跟name的作用一致
required| 是否是必须传的 默认true
defaultValue| 默认值（不传就是他）

ok，了解这个之后就可以去试试吧，改改这些属性值试试看吧！

**ps.如果参数上没加这个注解的话spring 默认name为参数名字，require为false**

##### 再次进阶
那么要是有很多参数怎么办？？？一个一个写不是疯了吗！那当然不能一个一个写，可以用对象来接受呀！

我们新建一个包``model``,再里面新建一个类 ``User``
```java
@Data
public class User {

    private String name;
    private Integer age;
    private String sex;

}
```
这边为了省事情用了lombok，这边留给你自己扩展，自己百度下怎么用哈哈哈哈
<br>再修改下``UserController``加个方法
```java
@RequestMapping("/pojo")
public User user(User user) {
    return user;
}
```
用postman测试会发现正确返回了提交的参数，不提交的就会为null

#### json流
json流那就更简单了，不过他只能接收json，还是改下``UserController``
```java
@RequestMapping("/pojo")
public User user(@RequestBody User user) {
    return user;
}
```
没错，只需要加上``@RequestBody``这个注解就行了！
<br>然后我们用postman测试下,这个比较特殊- -我还是贴个图吧,注意箭头指的那些
![json](../img/json.png)

**ps.如果加了@RequestBody注解还用key-value的方式传参的话，就会抛400**

#### 传文件就不说了，后面有用到的话再说
#### 路径参数
restful必备！不懂restful是啥就百度下，说的肯定比我好哈哈哈哈
<br>这个也很简单，举个栗子
```java
@GetMapping("/{name}")
public User getUser(@PathVariable(name = "name") String name) {
    User user = new User();
    user.setName(name);
    return user;
}
```
请求 **/user/October** 就会被映射到这个方法上，因为mapping里用了{name}表示url path这块是个参数，并且参数名是name
@PathVariable 就用来拿到这个参数，name是对应的，不写的话默认跟参数名一致，并且默认是必传的

路径参数一般是带有含义的，像比如上面写的这个url path **/user/{name}** ，而且是get方法，就表示获取name为XX的用户，具体的你看restful就懂了！

ok，到这这节课结束了，课后多练习巩固下，像比如路径参数可以试试多个呀（/{xx}/{yy}),或者(/{xx}/yy)这种，多玩玩

下节课就咱就开始引入数据库，用上mybatis-plus，mybatis就不讲了，一通百通都一样