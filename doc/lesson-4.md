# 第四课

这节课就简单的实现下一个员工管理系统，就两个表，员工表跟部门表，员工表关联部门表。很简单的

下面列出表结构
```sql
create table dept
(
    id        int auto_increment
        primary key,
    dept_name varchar(20) null,
    ct        datetime    null,
    mt        datetime    null
);
create table employ
(
    id      int auto_increment
        primary key,
    name    varchar(40) null,
    sex     varchar(5)  null,
    dept_id int         null,
    ct      datetime    null,
    mt      datetime    null
);
```
创建好表之后用之前写好的代码生成器生成下代码，忘了的话回去看！！！

### 页面准备
从这节课开始开始接触html+js,我就直接跳过jquery直接上vue了，先从直接引入vue.js开始，熟悉vue基础语法后我们再开始学spa

我们之前在template里面有新建过一个index.html，我们直接在这个页面上修改，在此之前先在controller包下新建一个``ViewControlelr``,用来返回页面

```java
@Controller
public class ViewController {

    @RequestMapping
    public String index() {
        return "index";
    }


}
```
接下来开始写页面，index.html就很简单两个a标签就好了自己看看吧