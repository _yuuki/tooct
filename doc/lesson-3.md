# 第三课

上节课咱学了参数接收的各种姿势，这节课开始进入正轨了，开始crud- -

### orm框架
java的orm框架很多，jpa、hibernate、mybatis等等等，咱也不一一讲了，就直接上mybatis了，用的多嘛

### 引入mybatis-plus
单纯用mybatis太难受了，所以用下他的增强包！

#### pom加依赖
```xml
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>3.3.0</version>
</dependency>
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
</dependency>
```
加上mybatis-plus的依赖还有mysql的驱动

#### 添加配置
```yaml
spring:
  datasource:
    type: com.zaxxer.hikari.HikariDataSource  #连接池
    driver-class-name: com.mysql.cj.jdbc.Driver #驱动
    url: jdbc:mysql://127.0.0.1:3306/oct?useUnicode=true&characterEncoding=utf8&allowMultiQueries=true&useSSL=false&serverTimezone=GMT%2B8  #mysql连接地址
    username: root
    password: root
    hikari:
      pool-name: demo
      minimum-idle: 5
      maximum-pool-size: 20
#mybatis plus 设置
mybatis-plus:
  type-aliases-package: com.yuuki.demo.gen.entity #别名扫描
  mapper-locations: classpath:mapper/*.xml  #xm扫描
  configuration:
    jdbc-type-for-null: null
  global-config:
    # 关闭 mybatis-plus的 banner
    banner: false

```
我这边把application.properties改成yml了，看着舒服，记得建数据库呀

#### 新建一张表
```sql
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `ct` datetime DEFAULT NULL,
  `mt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```
把建表语句放这了，就执行以下就好了
#### 启动类上加上注解
```java
@MapperScan(value = {"com.yuuki.demo.*.mapper"})
```
扫描mapper文件
#### 添加代码生成器
1. 添加依赖
    ```xml
        <dependency>
            <groupId>com.baomidou</groupId>
            <artifactId>mybatis-plus-generator</artifactId>
            <version>3.3.0</version>
        </dependency>
        <dependency>
            <groupId>org.freemarker</groupId>
            <artifactId>freemarker</artifactId>
            <version>2.3.29</version>
        </dependency>
    ```
2. 复制模板（在/resources/generator/templates下）我这边也是直接复制官方的
3. 生成的代码也是直接抄mybatis-plus官方文档的代码就好了，具体代码在 **generator** 包下，执行main方法，输入表名就生成了,非常的方便啊
4. 生成完xml文件在mapper.xml里，我们把他拉到resources/mapper下

#### 用idea连接数据库（先看着，后期可能有用）
idea右边有一个database的标签
<br>
![idea-1](../img/idea-database1.png)
![idea-2](../img/idea-database2.png)

然后输一下你的账号密码跟数据库就好了

#### 编写增删改查代码
mybatis-plus单表的增删改查非常的简单啊，emmm就看下gen包下的示例吧，一个一个贴好烦的，一般的web项目结构也就这样子了，自己试试吧~

当然，我写的是很粗糙的，只是让你了解下基本的结构还有增删改查的方法，还有，我用了restful风格的api，不知道你有么有去看。。。有去看的话应该就能理解为啥这样写哈哈哈

mybatis-plus还有很多方法，可以先看下官方的文档，我们下节课就。。。想想说啥- -

***ps.如果你完全按照我上面说的做的话，启动的时候应该会报错，这个很简单的就留给你自己解决啦哈哈哈哈***

最后附上我的测试结果
![curd](../img/curd1.png)
![curd](../img/curd2.png)
![curd](../img/curd3.png)
![curd](../img/curd4.png)
![curd](../img/curd5.png)